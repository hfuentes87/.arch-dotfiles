#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Se agrega branch de git a la prompt
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

# Inicio de import de variables
export PS1="\[\033[38;5;9m\]\w\[$(tput sgr0)\]\[\033[38;5;15m\]\n\[$(tput bold)\]\[$(tput sgr0)\]\[\033[38;5;42m\]\u\[$(tput sgr0)\]\[$(tput sgr0)\]\[\033[38;5;15m\]@\[$(tput bold)\]\[$(tput sgr0)\]\[\033[38;5;42m\]\h\[$(tput sgr0)\]\[$(tput sgr0)\]\[\033[38;5;15m\]\[\033[38;5;9m\]\$(parse_git_branch)\[$(tput sgr0)\] \\$\[$(tput sgr0)\] "
export PATH=$HOME/Dev/Flutter/flutter/bin:$HOME/.local/bin:$HOME/Applications:$PATH
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export GIT_EDITOR=vim
export PAGER=less
export EDITOR=vim

# Aliases
alias ls='exa'
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias cddd='cd $HOME/Dev/DentalSoft/'
alias cdd='cd $HOME/Dev/'
alias cdd10='cd $HOME/Dev/Dentalsoft/10'
alias cddh='cd $HOME/Dev/Dentalsoft/hoscar'
alias stsrv='php -S 127.0.0.1:8080'
alias stsrv2='php -S 127.0.0.1:8000'
alias psqldev='psql -U postgres -h 127.0.0.1'

complete -C /usr/bin/mcli mcli
