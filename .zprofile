[[ -f ~/.zshrc ]] && . ~/.zshrc

export TERMINAL="alacritty"
export PATH="$HOME/Applications/.npm-global/bin:$HOME/.cargo/bin:$PATH"
